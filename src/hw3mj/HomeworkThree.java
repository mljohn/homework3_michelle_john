/**
 * File: HomeworkThree
 * Author: Michelle John
 * Date: 02 December 2018
 * Purpose: Create a 3D scene with user interaction
 */
package hw3mj;

import com.jme3.app.SimpleApplication;
import com.jme3.font.BitmapText;
import com.jme3.font.LineWrapMode;
import com.jme3.font.Rectangle;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;

/**
 * Main class for this application. Extends {@link SimpleApplication}.
 */
public class HomeworkThree extends SimpleApplication {
    
    protected Node sphereNode;
    protected Node cubeNode;
    
    protected boolean isRunning = true;

    /**
     * Entry point into the application.
     * 
     * @param args the list of arguments used at the start
     */
    public static void main(String[] args) {
        HomeworkThree app = new HomeworkThree();
        app.start();
    }

    @Override
    public void simpleInitApp() {
        /** Increase the speed of the camera. Makes moving across the scene faster. */
        flyCam.setMoveSpeed(16f);
        Spatial gameLevel = assetManager.loadModel("Scenes/manyLights/Main.j3o");
        gameLevel.setLocalTranslation(0, -5.2f, 0);
        gameLevel.setLocalScale(2);
        
        /** Puts instructions on the screen. */
        guiNode.detachAllChildren();
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        String instructionText = "To rotate the spheres use keys E and R or the left and right mouse buttons. "
                + "To shift the cubes use keys X and C.";
        BitmapText instructions = new BitmapText(guiFont, false);
        instructions.setSize(guiFont.getCharSet().getRenderedSize());
        instructions.setText(instructionText);
        instructions.setBox(new Rectangle(0, 30, 450, 30));
        instructions.setLineWrapMode(LineWrapMode.Word);
        instructions.setLocalTranslation(200, instructions.getLineHeight(), 0);
        guiNode.attachChild(instructions);
      
        /** Create the spheres and attach them to {@code sphereNode}. */
        Spatial sphere1 = new Geometry("Sphere1", new Sphere(24, 24, 1f));
        Material mat_sphere1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat_sphere1.setColor("Color", ColorRGBA.Green);
        sphere1.setMaterial(mat_sphere1);
        sphere1.setLocalTranslation(new Vector3f(-1, 0, -1));
        
        Spatial sphere2 = new Geometry("Sphere2", new Sphere(24, 24, 1f));
        Material mat_sphere2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat_sphere2.setColor("Color", ColorRGBA.Cyan);
        sphere2.setMaterial(mat_sphere2);
        sphere2.setLocalTranslation(1, 0, -1);
        
        sphereNode = new Node("sphere");
        sphereNode.attachChild(sphere1);
        sphereNode.attachChild(sphere2);
        sphereNode.setLocalTranslation(11, 0, 0);
        
        /** Create the cubes and attach them to {@code cubeNode}. */
        Spatial cube1 = new Geometry("Cube_1", new Box(1, 1, 1));
        Material mat_Cube1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat_Cube1.setColor("Color", ColorRGBA.DarkGray);
        cube1.setMaterial(mat_Cube1);
        cube1.setLocalTranslation(new Vector3f(-1, 0, -1));
        
        Spatial cube2 = new Geometry("Cube_1", new Box(1, 1, 1));
        Material mat_Cube2 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat_Cube2.setColor("Color", ColorRGBA.LightGray);
        cube2.setMaterial(mat_Cube2);
        cube2.setLocalTranslation(new Vector3f(1, 0, -1));
        
        cubeNode = new Node("cube");
        cubeNode.attachChild(cube1);
        cubeNode.attachChild(cube2);
        cubeNode.setLocalTranslation(-11, 0, 0);
        
        /** Attach {@code sphereNode} and {@code cubeNode} to the {@code rootNode}. */
        rootNode.attachChild(gameLevel);
        rootNode.attachChild(sphereNode);
        rootNode.attachChild(cubeNode);
        initKeys();
    }
    
    /**
     * Creates and registers the mapping for input processing.
     */
    private void initKeys() {
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("Rotate_CC", new KeyTrigger(KeyInput.KEY_R),
                new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("Rotate_C", new KeyTrigger(KeyInput.KEY_E),
                new MouseButtonTrigger(MouseInput.BUTTON_RIGHT));
        
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_X));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_C));
        
        inputManager.addListener(actionListener, "Pause");
        inputManager.addListener(analogListener, "Rotate_CC", "Rotate_C", "Left", "Right");
    }
    
    private final ActionListener actionListener = new ActionListener() {
        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            if ("Pause".equals(name) && !keyPressed) isRunning = !isRunning;
        }
    };
    
    private final AnalogListener analogListener = new AnalogListener() {
        @Override
        public void onAnalog(String name, float value, float tpf) {
            Vector3f cubeVector = cubeNode.getLocalTranslation();
            if (isRunning) {
                if ("Rotate_CC".equals(name)) {
                    sphereNode.rotate(0, value * speed, 0);
                }
                if ("Rotate_C".equals(name)) {
                    sphereNode.rotate(0, -value * speed, 0);
                }
                if ("Left".equals(name)) {
                    cubeNode.setLocalTranslation(cubeVector.x - value * speed, cubeVector.y, cubeVector.z);
                }
                if ("Right".equals(name)) {
                    cubeNode.setLocalTranslation(cubeVector.x + value * speed, cubeVector.y, cubeVector.z);
                }
            } else {
                System.out.println("Game is paused.");
            }
        }
    };
}
